import React from 'react';
import axios from 'axios';

export default ({ postId }) => {
    const [content, setContent] = React.useState('');

    const onSubmit = async (event) => {
        event.preventDefault();

        // axios request
        await axios.post(`http://posts.com/posts/${postId}/comments`, {
            content: content
        });

        setContent('');
    };

    return <div>
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="comment">New Comment</label>
                <input type="text" className="form-control" name="comment" value={content} onChange={(event) => setContent(event.target.value)} />
            </div>
            <button className="btn btn-primary">SUBMIT</button>
        </form>
    </div>;
};