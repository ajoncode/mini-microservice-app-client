import React from 'react';
import axios from 'axios';

export default () => {
    const [title, setTitle] = React.useState('');

    const onSubmit = async (event) => {
        event.preventDefault();

        // axios request
        await axios.post('http://posts.com/posts/create', {
            title: title
        });

        setTitle('');
    };

    return <div>
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" name="title" value={title} onChange={(event) => setTitle(event.target.value)} />
            </div>
            <button className="btn btn-primary">SUBMIT</button>
        </form>
    </div>;
};