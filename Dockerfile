# Specify a base image
FROM node:12-alpine
ENV CI=true

WORKDIR /var/www/app

# Install dependencies
COPY ./package.json ./
RUN npm install
COPY ./ ./

# Default command
CMD ["npm", "start"]